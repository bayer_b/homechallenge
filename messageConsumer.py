from kafka import KafkaConsumer, TopicPartition

from Tools import extractValue, ExtractValueException, NoNewMessageException

import dash
from dash.dependencies import Output, Event, State, Input
import dash_core_components as dcc
import dash_html_components as html
from dash.exceptions import PreventUpdate
import plotly
import dash_table
import plotly.graph_objs as go

import logging
import sys
import traceback
import os
import datetime
import time

logging.basicConfig(format='%(levelname)s %(asctime)s %(message)s', stream=sys.stdout, level=logging.INFO)

kafkaHost = os.getenv('KAFKAHOST')
kafkaPort = os.getenv('KAFKAPORT')
kafkaTopic = os.getenv('KAFKATOPIC')
refreshInterval = int(os.getenv('REFRESHINTERVAL'))
kafkaConnStr = '{}:{}'.format(kafkaHost, kafkaPort)

time.sleep(int(os.getenv('WAITBEFORECONNECT')))

try:
    consumer = KafkaConsumer(bootstrap_servers=[kafkaConnStr], group_id='messageConsumer', enable_auto_commit=False, api_version=(0, 10, 1))
    topicPart = TopicPartition(kafkaTopic, 0)
    consumer.assign([topicPart])
except Exception as e:
    logging.error('%s error: %s', 'Kafka connection',traceback.format_exc())
    sys.exit(1)

app = dash.Dash(__name__)

app.css.config.serve_locally = True
app.scripts.config.serve_locally = True


app.layout = html.Div(
    [
        html.Div(id='live-timestamp', style={'color': 'blue', 'fontSize': 28, 'textAlign': 'right'}),
        dcc.Graph(id='live-graph', animate=False),
        dash_table.DataTable(
            id='live-alerts',
            columns =[
                {"name": "offset", "id": "offset"},
                {"name": "date", "id": "date"},
                {"name": "message", "id": "message"},
                {"name": "error", "id": "error"}
                ],
            style_cell={'textAlign': 'left'},
            editable=True
        ),
        dcc.Interval(
            id='layout-update',
            interval=refreshInterval*1000
        ),
        dcc.Store(id='session-store', storage_type='session')
    ]
)


@app.callback(
    Output('live-timestamp', 'children'),
    events=[Event('layout-update', 'interval')]
    )
def updateTimeStamp():
    timeStampFormat = "%H:%M:%S"
    timeStamp = datetime.datetime.now().strftime(timeStampFormat)
    return "updated at: {}".format(timeStamp)


@app.callback(
    Output('session-store', 'data'),
    events=[Event('layout-update', 'interval')],
    state=[State('session-store', 'data')]
    )
def updateStore(sessionData):
    if sessionData is None:
        sessionData = {}
        sessionData['X'] = []
        sessionData['YSumOf'] = []
        sessionData['YNumOf'] = []
        sessionData['alerts'] = []

    sumOf = 0
    numOf = 0
    
    startPos = consumer.position(topicPart)
    nextStartPos = consumer.end_offsets([topicPart])[topicPart]
    consumer.seek(topicPart, startPos)

    try:
        if startPos == nextStartPos:
            raise NoNewMessageException('')

        for message in consumer:
            if message.offset == nextStartPos - 1:
                consumer.seek(topicPart, nextStartPos - 1)
                break
            else:
                try:
                    sumOf = sumOf + extractValue(message.value)
                    numOf = numOf + 1
                except ExtractValueException as error:
                    timeStampFormat = "%H:%M:%S"
                    alertTimeStamp = datetime.datetime.now().strftime(timeStampFormat)
                    sessionData['alerts'].append(
                        {
                            'offset': message.offset,
                            'date': alertTimeStamp,
                            'message': message.value,
                            'error': str(error)
                        }
                        )
            
        sessionData['X'].append(datetime.datetime.now().strftime('%H:%M:%S'))
        sessionData['YSumOf'].append(sumOf)
        sessionData['YNumOf'].append(numOf)
    except NoNewMessageException:
        logging.info("No new messages arrived")
        raise PreventUpdate

    return sessionData


@app.callback(
    Output('live-graph', 'figure'),
    [Input('session-store', 'modified_timestamp')],
    state=[State('session-store', 'data')]
    )
def updateGraph(sessionStoreTimeStamp, sessionData):
    if sessionStoreTimeStamp is None:
        raise PreventUpdate
    if sessionData is None:
        raise PreventUpdate

    dataSumOf = plotly.graph_objs.Scatter(
        x = list(sessionData['X']),
        y = list(sessionData['YSumOf']),
        name = 'Sum of values',
        mode = 'lines+markers'
    )

    dataNumOf = plotly.graph_objs.Bar(
        x = list(sessionData['X']),
        y = list(sessionData['YNumOf']),
        width = 0.1,
        name = 'Number of values',
        yaxis = 'y2',
        opacity=0.5,
    )    

    return {
        'data': [dataSumOf, dataNumOf],
        'layout': plotly.graph_objs.Layout(
            yaxis=dict(title='Sum of values'),
            xaxis=dict(title='Time'),
            yaxis2=dict(title='Number of values', overlaying='y', side='right'),
            autosize=True,
            showlegend=True
        )
            
    }

@app.callback(
    Output('live-alerts', 'data'),
    [Input('session-store', 'modified_timestamp')],
    state=[State('session-store', 'data')]
    )
def updateTable(sessionStoreTimeStamp, sessionData):
    if sessionStoreTimeStamp is None:
        raise PreventUpdate
    if sessionData is None:
        raise PreventUpdate

    return sessionData['alerts']

if __name__ == '__main__':
    app.run_server()

