# Usage

Execute docker-compose up -d in the folder of the docker-compose file I sent you via email.

Right after the start the python script responsible for the message consuming and UI in the message-consumer container waits 15 seconds before connecting to kafka.
This value can be changed in the docker-compose file by adjusting the WAITBEFORECONNECT variable.

To see th web UI point your browser to http://127.0.0.1:8050

# UI explanation

![picture](ui.png)

The layout refreshes every 10 seconds. This value can be changed in the docker-compose file by adjusting the REFRESHINTERVAL variable.
On the graph you can see the sum and the number of values within the 10 seconds timeframe. In the table below the messages are visible, that could not be processed.
