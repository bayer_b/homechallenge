import json



class ExtractValueException(Exception):
    pass

class NoNewMessageException(Exception):
    pass

def extractValue(valueS):
	try:
		valueJSON = json.loads(valueS)
		value = int(valueJSON.get('count'))
	except TypeError as error:
		raise ExtractValueException(str(error))
	except ValueError as error:
		raise ExtractValueException(str(error))
	return value

	
