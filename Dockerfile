FROM python:3

RUN pip install kafka-python dash dash-core-components dash-html-components dash-renderer dash-table

RUN mkdir /app
ADD messageConsumer.py /app/
ADD Tools.py /app/

CMD ["python", "/app/messageConsumer.py"]
